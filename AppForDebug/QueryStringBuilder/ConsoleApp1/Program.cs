﻿// See https://aka.ms/new-console-template for more information

using System.Collections;

var filter = new Filter()
{
    RecruiterId = 1,
    Type = CandidateType.Staff,
    EmploymentTypes = new List<CandidateType> { CandidateType.Staff, CandidateType.Travel }
};

string separator = ",";

var result = new List<KeyValue>();
bool isEnum;

var properties = filter.GetType().GetProperties()
            .Where(x => x.CanRead)
            .Where(x => x.GetValue(filter, null) != null)
            .ToDictionary(x => x.Name, x => x.GetValue(filter, null));

var simpleValues =
    properties
    .Where(x => x.Value.GetType().IsPrimitive || x.Value.GetType() == typeof(string))
    .Select(x => {
        return new KeyValue { Name = x.Key, Value = x.Value.ToString() };
    });
result.AddRange(simpleValues);

var enumValues =
properties
    .Where(x => x.Value.GetType().IsEnum)
    .Select(x =>
    {
        var val = GetEnumValue(x.Value.GetType(), x.Value.ToString());
        return new KeyValue { Name = x.Key, Value = val.ToString() };
    });
result.AddRange(enumValues);

var enumerableValues =
    properties
    .Where(x => !(x.Value is string) && x.Value is IEnumerable)
    .Select(x => x.Key)
    .ToList();

foreach (var key in enumerableValues)
{
    var valueType = properties[key].GetType();
    var valueElemType = valueType.IsGenericType
                            ? valueType.GetGenericArguments()[0]
                            : valueType.GetElementType();
    if (valueElemType.IsPrimitive)
    {
        var enumerable = properties[key] as IEnumerable;
        foreach (var item in enumerable)
        {
            result.Add(new KeyValue { Name = key, Value = item.ToString() });
        }
    }
    if (valueElemType.IsEnum)
    {
        var enumerable = properties[key] as IEnumerable;
        List<string> res = new List<string>();
        foreach (var item in enumerable)
        {
            result.Add(new KeyValue
            {
                Name = key,
                Value = GetEnumValue(valueElemType, item.ToString()).ToString()
            });
        }
    }

}

foreach (var pr in result) {
    Console.WriteLine($"Name {pr.Name}, Value {pr.Value}");
}

int GetEnumValue(Type enumType, string value)
{
    Enum enumValue = Enum.Parse(enumType, value) as Enum;
    return Convert.ToInt32(enumValue);
}


public enum CandidateType
{
    Staff = 1,
    Travel = 2,
    PerDiem = 3,
    Strike = 5,
    EMR = 6,
    Locums = 7,
    WorkforceDisruption = 9
}

public class Filter
{

    public List<CandidateType> EmploymentTypes { get; set; }

    public CandidateType Type { get; set; }

    public int RecruiterId { get; set; }

}

public class KeyValue
{

    public string Name { get; set; }
    public string Value { get; set; }

}