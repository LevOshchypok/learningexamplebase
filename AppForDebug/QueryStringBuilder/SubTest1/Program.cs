﻿// See https://aka.ms/new-console-template for more information
Console.WriteLine("Hello, World!");

var r = CandidateType.Locums;

//Console.WriteLine(r.ToString());
//Console.WriteLine((int)r);

getEnumValue<CandidateType>(r);


void getEnumValue<T>(T val)
    where T: Enum
{
    Console.WriteLine(val.ToString());

    Enum enumValue = Enum.Parse(typeof(T), val.ToString()) as Enum;
    Console.WriteLine(Convert.ToInt32(enumValue));
}


public enum CandidateType
{
    Staff = 1,
    Travel = 2,
    PerDiem = 3,
    Strike = 5,
    EMR = 6,
    Locums = 7,
    WorkforceDisruption = 9
}
