﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace DeadlockSimulation
{
    internal class Program
    {
        static readonly object firstLock = new object();
        static readonly object secondLock = new object();


        // # defintion of the deadlock
        //deadlock is a situation
        //when two processes each having a lock on the one source 
        //and the attempt to acquire a lock on the other resource.


        static void Main(string[] args)
        {
            Task.Run((Action)Do);

            Thread.Sleep(500);
            Console.WriteLine($"\t\t{Thread.CurrentThread.ManagedThreadId} -- Locking seecondLock");
            lock (secondLock)
            {
                Console.WriteLine($"\t\t{Thread.CurrentThread.ManagedThreadId} -- Locked secondLock");

                lock (firstLock)
                {
                    Console.WriteLine($"\t\t{Thread.CurrentThread.ManagedThreadId} -- Locked firstLock");
                }

                Console.WriteLine($"\t\t{Thread.CurrentThread.ManagedThreadId} -- Released firstLock");

            }

            Console.WriteLine($"\t\t{Thread.CurrentThread.ManagedThreadId} -- Released secondLock");

            Console.Read();
        }

        private static void Do()
        {
            Console.WriteLine($"\t\t\t\t{Thread.CurrentThread.ManagedThreadId} -- Locking firstLock");
            lock(firstLock)
            {
                Console.WriteLine($"\t\t\t\t{Thread.CurrentThread.ManagedThreadId} -- Locked firstLock");

                Thread.Sleep(1000);

                Console.WriteLine($"\t\t\t\t{Thread.CurrentThread.ManagedThreadId} -- Locking seecondLock");
                lock(secondLock)
                {
                    Console.WriteLine($"\t\t\t\t{Thread.CurrentThread.ManagedThreadId} -- Locked secondLock");
                }

                Console.WriteLine($"\t\t\t\t{Thread.CurrentThread.ManagedThreadId} -- Released secondLock");
            }

            Console.WriteLine($"\t\t\t\t{Thread.CurrentThread.ManagedThreadId} -- Released firstLock");
        }
    }
}
