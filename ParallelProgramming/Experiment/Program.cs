﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Experiment
{

    class Dict {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public enum BookingStatusEnum : byte
    {
        None = 0,
        Onsite = 1,
        BookingTravel = 2,
        BookingIssues = 3,
        BookingIssuesResolved = 4,
        TravelBooked = 5,
        TravelSent = 6,
        TravelCancelled = 7
    }

    internal class Program
    {
        static void Main(string[] args)
        {


            //List<Dict> statusLookup = BookingStatusEnum.GetValues(typeof(BookingStatusEnum))

            //List<Dict> statusLookup = BookingStatusEnum.GetValues(typeof(byte))
            //  .Cast<byte>()
            //  .Select(e => new Dict { Id = e, Name = e  })
            //  .ToList();

            List<Dict> statusLookup = BookingStatusEnum.GetValues(typeof(BookingStatusEnum))
              .Cast<byte>()
              .Select(e => new Dict { Id = (int)e, Name = BookingStatusEnum.GetName(typeof(BookingStatusEnum), e) })
              .ToList();


            //var statusLookup = BookingStatusEnum.GetValues(typeof(BookingStatusEnum))
            //                                    .Select(x => new Dict { Id = (byte)x, Name = x })
            //                                    .ToList();

            List<Dict> d = new List<Dict>(); 
            foreach (var el in BookingStatusEnum.GetValues(typeof(BookingStatusEnum)))
            {
                // d.Add(new Dict { Id = (byte)el, Name = el.ToString() });
                Console.WriteLine(el);
            }

            Console.WriteLine("Hello World!");

            Console.Read();
        }
    }
}
