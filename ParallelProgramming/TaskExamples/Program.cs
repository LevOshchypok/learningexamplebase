﻿using System;
using System.Threading;
using System.Threading.Tasks;

namespace TaskExamples
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // simplest way to create a task 
            /*
            Task.Run(() => Print());
            Task.Run(() => Print());
            */

            // it is the same if we will create task in the next way 
            Task<int> t1 = Task.Factory.StartNew(() => Print(), 
                CancellationToken.None, // to cancel 
                TaskCreationOptions.DenyChildAttach,
                TaskScheduler.Default);

            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");
            Console.WriteLine("Some intermediate work");


            Console.WriteLine($"Result of the your task is {t1.Result}");
        }

        private static int Print()
        {
            for(int i = 0; i < 100; i++)
            {
                Console.WriteLine(i.ToString());
            }

            Thread.Sleep(10000);

            return 10;
        }
    }
}

