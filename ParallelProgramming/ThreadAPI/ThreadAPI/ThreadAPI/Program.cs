﻿using System;
using System.Threading;

namespace ThreadAPI
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Thread t1 = new Thread(PrintEven);

            t1.Start();

            PrintOdd();

            Console.WriteLine("Hello World!");
            Console.WriteLine($"Current thread ID:{Thread.CurrentThread.ManagedThreadId}");
        }

        private static void PrintEven() { 
        
            for(int i =0; i < 100; i++)
            {
                if(i%2 == 0)
                {
                    System.Console.WriteLine(i);
                }
            }
        }

        private static void PrintOdd()
        {

            for (int i = 0; i < 100; i++)
            {
                if (i % 2 != 0)
                {
                    System.Console.WriteLine(i);
                }
            }
        }
    }
}
