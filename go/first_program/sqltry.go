package main

import (
	"database/sql"
	"fmt"

	_ "github.com/denisenkom/go-mssqldb" // SQL Server driver for database/sql
)

func main() {
	// Connect to the database
	connString := "localhost\\SQLEXPRESS;database=Go;integrated security=true"

	db, err := sql.Open("sqlserver", connString)
	if err != nil {
		fmt.Println("Error connecting to database:", err)
		return
	}
	defer db.Close()

	// Query the user table
	rows, err := db.Query("SELECT id, name FROM users")
	if err != nil {
		fmt.Println("Error querying database:", err)
		return
	}
	defer rows.Close()

	// Iterate over the results and print them
	for rows.Next() {
		var id int
		var name string
		if err := rows.Scan(&id, &name); err != nil {
			fmt.Println("Error scanning row:", err)
			return
		}
		fmt.Printf("User %d: %s\n", id, name)
	}
	if err := rows.Err(); err != nil {
		fmt.Println("Error iterating over rows:", err)
		return
	}
}
