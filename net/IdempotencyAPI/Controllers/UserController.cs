using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace IdempotencyAPI.Controllers;

[ApiController]
[Route("[controller]")]
public class UserController : ControllerBase
{
    private readonly ILogger<UserController> _logger;
    private readonly MyDbContext _context;

    public UserController(ILogger<UserController> logger, MyDbContext context)
    {
        _logger = logger;
        _context = context;
    }

    [HttpGet(Name = "GetUser")]
    public async Task<List<User>> Get()
    {
        return await _context.Users.ToListAsync();
    }

    [HttpPost(Name = "PostUser")]
    public async Task<User> Post([FromBody] User user)
    {
        await _context.Users.AddAsync(user);
        await _context.SaveChangesAsync();
    
        return user;
    }
}