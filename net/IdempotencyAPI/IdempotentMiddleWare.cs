public class IdempotentMiddleWare
{
    private readonly RequestDelegate _next;
 
    public IdempotentMiddleWare(RequestDelegate next)
    {
        this._next = next;
    }
 
    public async Task InvokeAsync(HttpContext context)
    {
        // Access the request headers
        var headers = context.Request.Headers;
        // get token from headers 
        // validate token from request 
        // store in the memory and call next if ok 
        // return 200 but not process request 

        await _next.Invoke(context);
    }
}