﻿using System;
using System.Data.SqlClient;


internal class Program
{
    private static void Main(string[] args)
    {
        string server = "localhost\\SQLEXPRESS";
        string database = "Go";
        string connString = $"Server={server};Database={database};Integrated Security=True;";

        // Connect to the database
        using (SqlConnection conn = new SqlConnection(connString))
        {
            conn.Open();

            // Query the user table
            using (SqlCommand cmd = new SqlCommand("SELECT id, name FROM users", conn))
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                // Iterate over the results and print them
                while (reader.Read())
                {
                    int id = reader.GetInt32(0);
                    string name = reader.GetString(1);
                    Console.WriteLine($"User {id}: {name}");
                }
            }
        }
    }
}