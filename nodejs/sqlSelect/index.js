const sql = require('mssql/msnodesqlv8')

async function main() {
  // Connect to the database
  const config = {
    connectionString: 'Driver=SQL Server;Server=localhost\\SQLEXPRESS;Database=Go;Trusted_Connection=true;'
  }
  try {
    await sql.connect(config)
    console.log('Connected to database')

    // Query the user table
    const result = await sql.query('SELECT id, name FROM users')
    const rows = result.recordset

    // Iterate over the results and print them
    for (const row of rows) {
      console.log(`User ${row.id}: ${row.name}`)
    }
  } catch (err) {
    console.log('Error querying database:', err)
  } finally {
    // Close the database connection
    await sql.close()
    console.log('Connection closed')
  }
}

main()