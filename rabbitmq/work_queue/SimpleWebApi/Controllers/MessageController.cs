using Microsoft.AspNetCore.Mvc;
using SimpleWebApi.DTO;
using SimpleWebApi.RabbitMQ;

namespace SimpleWebApi.Controllers;

[ApiController]
[Route("[controller]")]
public class MessageController : ControllerBase
{
    private readonly ILogger<MessageController> _logger;
    private readonly IRabitMQProducer _producer;

    public MessageController(ILogger<MessageController> logger, 
    IRabitMQProducer producer)
    {
        _logger = logger;
        _producer = producer;
    }

    [HttpPost]
    public IActionResult PostMessage([FromBody] MessageDto message)
    {
        _producer.SendProductMessage<MessageDto>(message);
        return Ok();
    }
}
