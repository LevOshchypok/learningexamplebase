namespace SimpleWebApi.DTO {
    public class MessageDto {
        public string Text { get; set; }
    }
}