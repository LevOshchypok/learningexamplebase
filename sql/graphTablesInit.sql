CREATE DATABASE Royalty;
GO

USE Royalty;
GO

CREATE TABLE Person (
    ID VARCHAR(500) NOT NULL,
    [Name] VARCHAR(500)
    ) AS NODE;

CREATE TABLE House (
    ID INTEGER NOT NULL,
    [House Name] VARCHAR(200)
    ) AS NODE;

CREATE TABLE Throne (
    ID INTEGER NOT NULL,
    [Throne of] VARCHAR(100),
    [Capital] VARCHAR(100)
    ) AS NODE;

CREATE TABLE SpouseOf AS EDGE;
CREATE TABLE ParentOf ([Parent Type] varchar(25)) AS EDGE;
CREATE TABLE Occupied ([Begin Date] DATE, [End Date] DATE) AS EDGE;
CREATE TABLE BelongsTo AS EDGE;
GO


select * from Person 
select * from ParentOf 


select * from Occupied

INSERT INTO dbo.Person (ID, [Name])
VALUES ('Queen_Victoria','Queen Victoria')
    ,('Victoria,_Princess_Royal','Princess Victoria Adelaide Mary Louisa')
    ,('Prince_Edward,_Duke_of_Kent_and_Strathearn','Prince Edward Augustus')
    ,('Princess_Victoria_of_Saxe-Coburg-Saalfeld','Princess Marie Luise Viktoria')


select * from ParentOf 

INSERT INTO dbo.ParentOf VALUES
    ((SELECT $node_id FROM Person WHERE ID = 'Queen_Victoria'), 
	(SELECT $node_id FROM Person WHERE ID = 'Princess_Alice_of_the_United_Kingdom'), 
	'Mother')

	INSERT INTO dbo.ParentOf VALUES
    ((SELECT $node_id FROM Person WHERE ID = 'Princess_Victoria_of_Saxe-Coburg-Saalfeld'), 
	(SELECT $node_id FROM Person WHERE ID = 'Queen_Victoria'), 
	'Mother')

    INSERT INTO dbo.ParentOf VALUES
	((SELECT $node_id FROM Person WHERE ID = 'Queen_Victoria'), 
	(SELECT $node_id FROM Person WHERE ID = 'Victoria,_Princess_Royal'), 
	'Mother')

	SELECT Person.[Name], Throne.[Throne of]
FROM dbo.Occupied, dbo.Person, Throne
