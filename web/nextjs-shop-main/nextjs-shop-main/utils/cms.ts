import Product from '@/types/Product';

type GetProductsParams = {
  page?: number;
  filterParams?: {
    search?: string;
    category?: string;
    sort?: string;
    inStock?: boolean;
  };
};

interface Pager {
  totalPage: number, 
  totalEntries: number,
  pageNum: number, 
  pageSize: number
}

type GeneralOutput<T> = {
  data: T[],
  paging: Pager
}

// export const getProducts = async (params: GetProductsParams) => {
//   const {
//     page = 1,
//     filterParams: {
//       search = '',
//       category = 'all',
//       sort = 'id',
//       inStock = false,
//     } = {},
//   } = params;

//   let res;
//   try{
//     res = await fetch(process.env.API_URL + '/data.json');
//   } catch (error)
//   {
//     console.error('Error parsing JSON:', error);
//   }
  
//   const products: Product[] = await res.json();

//   if (!res.ok) {
//     throw new Error('Failed to fetch data');
//   }

//   let filteredItems = products;

//   filteredItems = products.filter((product: Product) => {
//     return (
//       product.name.toLowerCase().includes(search.toLowerCase()) ||
//       product.imageAlt.toLowerCase().includes(search.toLowerCase())
//     );
//   });

//   if (category !== 'all') {
//     filteredItems = filteredItems.filter(
//       product => product.category === category
//     );
//   }

//   if (sort === 'name-asc') {
//     filteredItems.sort((a: Product, b: Product) => {
//       return a.name.localeCompare(b.name);
//     });
//   } else if (sort === 'name-desc') {
//     filteredItems.sort((a: Product, b: Product) => {
//       return b.name.localeCompare(a.name);
//     });
//   } else if (sort === 'price-asc') {
//     filteredItems.sort((a: Product, b: Product) => {
//       return a.price - b.price;
//     });
//   } else if (sort === 'price-desc') {
//     filteredItems.sort((a: Product, b: Product) => {
//       return b.price - a.price;
//     });
//   } else {
//     filteredItems.sort((a: Product, b: Product) => {
//       return a.id - b.id;
//     });
//   }

//   if (inStock) {
//     filteredItems = filteredItems.filter((product: Product) => {
//       return product.inStock;
//     });
//   }

//   let result = filteredItems;

//   if (page !== -1) {
//     result = filteredItems.slice((page - 1) * 4, page * 4);
//   }

//   const total = filteredItems.length;

//   return { result, total };
// };

export const getProducts = async (params: GetProductsParams) => {
  const request = await fetch(process.env.API_URL + '/client/service');
  const result: GeneralOutput<Product> = await request.json();
  const products: Product[] = result.data; 

  return {
    result: products, 
    total: result.paging.totalEntries
  }
}

export const getProductCategories = async () => {
  const request = await fetch(process.env.API_URL + '/client/categories');
  const res: any[] = await request.json();
  const categories = res.map(cat => cat.name);
  
  return ['all', ...new Set(categories)]

}

// export const getProductCategories = async () => {
//   //const res = await fetch(process.env.API_URL + '/data.json');
//   let res;
//   try{
//     res = await fetch(process.env.API_URL + '/data.json');
//   } catch (error)
//   {
//     console.error('Error parsing JSON:', error);
//   }
//   const products: Product[] = await res.json();

//   if (!res.ok) {
//     throw new Error('Failed to fetch data');
//   }

//   const categories = products.map(product => product.category);

//   return ['all', ...new Set(categories)];
// };

export const getProductBySlug = async (slug: string) => {
  // const res = await fetch(process.env.API_URL + '/data.json');
  // const products: Product[] = await res.json();

  // if (!res.ok) {
  //   throw new Error('Failed to fetch data');
  // }

  // const product = products.find(product => product.link === slug);

  return {
    id: 7,
    name: "Brass Scissors",
    link: "brass-scissors",
    price: 50,
    category: "scissors",
    inStock: true,
    imageSrc: "/images/product-07.jpg",
    imageAlt: "Brass scissors with geometric design, black steel finger holes, and included upright brass stand.",
    description: "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Tempore modi dignissimos deleniti repellat sint ipsum minima quo voluptate. Eaque numquam at vero blanditiis commodi ratione, iure ipsam veritatis? Fuga, illo. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ullam temporibus veritatis odio praesentium maiores possimus amet inventore eius officia voluptatum rerum impedit tenetur ipsam vero blanditiis, soluta assumenda, enim dolorem."
  } as Product;
};

export const getProductById = async (id: number) => {
  const request = await fetch(process.env.API_URL + '/client/service')
  const product: Product = await request.json();

  return product;
};

// export const getProductById = async (id: number) => {
//   const res = await fetch(process.env.API_URL + '/data.json');
//   const products: Product[] = await res.json();

//   if (!res.ok) {
//     throw new Error('Failed to fetch data');
//   }

//   const product = products.find(product => product.id === id);

//   return product;
// };
