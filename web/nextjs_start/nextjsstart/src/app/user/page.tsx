import QuestionForm from "@/components/question/question-form";
import QuestionList from "@/components/question/question-list/question-list";

export default function Dashboard() {
    return (
        <div>
            <h1>Dashboard page testing</h1>
            <QuestionList topicRef={1}/>
        </div>
    )
}