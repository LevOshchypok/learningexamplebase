import axios from 'axios';
import {api_config} from '../config/index';

export function request(url: string, options: any = {}){
    const config = {
        method:'GET',
        mode:'cors',
        ...options
    };

    const errors = [];

    if (!url){
        errors.push('url');
    }
    if(!config.payload && (config.method !== 'GET' && config.method !== 'DELETE')){
        errors.push('payload');
    }
    if (errors.length) {
        throw new Error(`Error! You must pass \`${errors.join('`, `')}\``);
    }
   
    const headers = {
        Accept: 'application/json',
        'Content-Type': 'application/json',
        ...config.headers,
    };

    const params = {
        headers,
        method: config.method,
        mode:config.mode,
        params: config.params || null,
        data: config.payload || null
    };
    
    console.log("url", url);
    return axios({
        url: /^(https?:\/\/)/.test(url) ? url : `${api_config.apiUrl}${url}`,
        ...params
    })
    .then(async response => {
        console.log('request response', response)
        
        if (response.status === 400 || response.status === 401)
          throw response;
       
        return response;
    })
    .catch(async err => {
        throw err;
    })  
}