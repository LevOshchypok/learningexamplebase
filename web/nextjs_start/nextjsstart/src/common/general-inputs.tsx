import TextField from '@mui/material/TextField';
import { FormControlLabel, Checkbox } from '@mui/material';


export const CTText = ({name, label, onChange, value, readonly, style,  className}: any) => {
    
    const defaultStyle = {
        margin:"5px"
    }

    const finalStyle = {...defaultStyle, style}

    return (
     <TextField
     className={className}
     id={name}
     label={label}
     name={name}
     InputProps={{
       autoComplete: "off", 
       readOnly: readonly ? readonly : false
     }}
     type='text'
     size="small"
     variant="outlined"
     onChange={onChange}
     value={value}
     style={finalStyle}
     />)
 }


 export const CTCheckBox = ({name, label, onChange, value }: any) => {

    return(
      <FormControlLabel
      control={
        <Checkbox
          checked={value}
          onChange={onChange}
          name={name}
          color="primary"
        />
      }
      label={label}
    />
    )
  }