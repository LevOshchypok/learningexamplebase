"use client";

import { Button, Card, CardActions, CardContent } from "@mui/material";
import React, { Component } from "react";
import { CTText } from "../../common/general-inputs";
import NotifyService from "../../common/notify-service";
import { Answer, Question } from "./question-models";
import QuestionService from "./question-service";
import './question-form.css'
import { AnswerForm } from "./answer-form";


type MyState = {
    questionText: string,
    answers: Answer[]
}

class QuestionForm extends Component<Readonly<any>, MyState>{
    emptyAnswer: Answer = {
        text: '',
        isCorrect: false
    }

    forceCleanup = false;

    state: MyState =
        {
            questionText: '',
            answers: []
        }

    componentDidMount(): void {
        this.setState({ answers: this.initEmptyAnswewrs(4) })
    }

    handleText = (e: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({ questionText: e.target.value })
    }

    initEmptyAnswewrs = (qty: number): Answer[] => {
        let answers = [];
        for (let i = 1; i <= qty; i++) {
            let answer: Answer = {
                orderNum: i,
                text: '',
                isCorrect: false
            }
            answers.push(answer);
        }
        return answers;
    }

    cleanupForm = () => {
        this.forceCleanup = true
        this.setState({
            questionText: '',
            answers: this.initEmptyAnswewrs(4)
        }, () => {
            this.forceCleanup = false
        })
    }

    addAnswer = () => {
        const newAnswer: Answer = {
            text: '',
            isCorrect: false
        }
        let answers = [...this.state.answers];
        newAnswer.orderNum = answers.length + 1;
        answers.push(newAnswer);
        this.setState({ answers: answers })
    }

    updateAnswerByNumber = (num: number) => {
        return (message: any) => {
            let answers = [...this.state.answers]
            answers.filter(x => x.orderNum === num)[0] =
            {
                text: message.text,
                isCorrect: message.isCorrect,
                orderNum: num
            }
            this.setState({ answers: answers })
        }
    }

    postQuestion = () => {
        const question: Question = {
            text: this.state.questionText,
            topicRef: 1,
            answers: this.state.answers.filter(x => x.text !== '')
        }

        QuestionService.postQuestion(question)
            .then((res) => {
                let message = `new question with id ${res.data} successfully created`
                NotifyService.showInfoMessage(message);
                this.cleanupForm();
            })
            .catch((error) => {
                let message = "error occured"
                NotifyService.showInfoMessage(message);
            })
    }

    render() {
        return (
            <Card className="question-form-body" >
                <CardContent className="question-form-content">
                    <h3>Зареєструвати запитання</h3>
                    <CTText
                        name="text"
                        className="question-text"
                        label="Текст питання"
                        value={this.state.questionText}
                        onChange={this.handleText}
                    />
                    <div className="question-form-answers">
                        {this.state.answers.map(ans =>
                            <AnswerForm key={ans.orderNum} 
                            orderNum={ans.orderNum} 
                            onChange={this.updateAnswerByNumber(ans.orderNum ?? 0)}
                            forceCleanup={this.forceCleanup} />)
                        }
                    </div> 
                    <Button variant="text" onClick={this.addAnswer}> Додати нову відповідь </Button>
                    <CardActions className="question-actions">             
                     <Button onClick={this.postQuestion}> Створити питання </Button>
                    </CardActions>
                </CardContent>
            </Card>)
    }
}
export default QuestionForm

