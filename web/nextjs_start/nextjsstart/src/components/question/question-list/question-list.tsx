"use client";

import { Card, CardContent } from "@mui/material"
import { Component } from "react"
import { Question } from "../question-models"
import QuestionService from "../question-service"
import './question_list.css'


type MyState = {
    questions: Question[], 
    answers: any[]
}

type MyProps = {
    topicRef: number
}

class QuestionList extends Component<MyProps, MyState>{

    state: MyState = {
        questions: [], 
        answers: []
    }

    getQuestions = (topicRef: number) => {
        QuestionService.getQuestions(topicRef)
        .then(res => {
            console.log("res with question", res.data)
            this.setState({questions: res.data.data})
        })
    }

    componentDidMount(): void {
        this.getQuestions(this.props.topicRef)
    }

    getAnswer = (quesionId: number, answerId: number) => {
        this.state.answers.push({quesionId: quesionId, answerId: answerId})
        console.log("I am here inside getAnswer");
    }

    render() {
        const {topicRef} = this.props
        return (<div className="question-list-root">
            <h1>Питання</h1>
            <div className="question-list-items">
                {this.state.questions.map(q => <QuestionItem key={q.id} question={q} callback={this.getAnswer}/>)}
            </div>
        </div>)
    }

}

export default QuestionList

interface QuestionItem {
    question: Question
}

const QuestionItem = ({question}: QuestionItem, callback: (qid: number, ansid: number) => void) => {

    return (<Card key={question.id}>
        <CardContent>
        <h3>{question.text}</h3>
        <form>
            {question.answers.map(ans =>
                <div key={ans.id}>
                    <input type="radio" name="question" value={ans.id} id={ans.id?.toString()}
                    onChange={()=>callback(question.id, ans.id)}
                    />
                    <label>{ans.id} {ans.text}</label>
                </div>)}
        </form>
        </CardContent>
    </Card>)
}