import { useState } from 'react';
import Link from 'next/link';
import Head from 'next/head';



function Header({ title }) {
  return <h1>{title ? title : "Default title"}</h1>
}

export default function HomePage() {
  const names = ["Ada Lovelace", "Grace Hopper", "Margaret Hamilton"]

  const [likes, setLikes] = useState(0)

  function handleClick() {
    setLikes(likes + 1)
  }

  return (
    <>
    <Head>
      <title>Levs test app</title>
      <link rel="icon" type="image/x-icon" href="/images/favicon.ico"/>
    </Head>
    <div>
      <Header title="Develop. Preview. Ship. 🚀" />
      <ul>
        {names.map((name) => (
          <li key={name}>{name}</li>
        ))}
      </ul>
      Read <Link href="/posts/first-post">this page!</Link>

      <button onClick={handleClick}>Like ({likes})</button>
    </div>
    </>
  )
}

